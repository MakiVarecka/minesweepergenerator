﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MinesweeperGenerator.Test")]
namespace MinesweeperGenerator
{
    internal class Game
    {
        private readonly int _rows = 10;
        private readonly int _cols = 20;
        private readonly int _minesCount = 15;

        private static int[,] _board;
        private static int[][] _mines;

        private bool IsInBoard(int x, int y) => (x >= 0) && (y >= 0) && (x < _rows) && (y < _cols);
        private static bool IsMine(int value) => value == -1;
        private static bool IsNotMine(int value) => value != -1;
        private static bool IsNotEmpty(int value) => value != 0;

        internal Game()
        {
            Prepare();
            GenerateMines();
            GenerateClue();
        }

        internal Game(int rows, int cols, int minesCount)
        {
            _rows = rows;
            _cols = cols;
            _minesCount = minesCount;

            Prepare();
            GenerateMines();
            GenerateClue();
        }

        private void Prepare()
        {
            if (_rows <= 0) throw new ArgumentException("Number of rows cannot be less or equal to 0.");
            if (_cols <= 0) throw new ArgumentException("Number of columns cannot be less or equal to 0.");
            if (_minesCount <= 0) throw new ArgumentException("Number of mines cannot be less or equal to 0.");
            if (_minesCount > _rows * _cols) throw new ArgumentException("Number of mines cannot be greater than number of rows multiply by number of columns.");

            _board = new int[_rows, _cols];
            _mines = new int[_minesCount][];
        }

        private void GenerateMines()
        {
            var rnd = new Random();
            var minesPlanted = 0;
            while (minesPlanted < _minesCount)
            {
                var x = rnd.Next(0, _rows - 1);
                var y = rnd.Next(0, _cols - 1);

                if (IsMine(_board[x, y])) continue;

                _board[x, y] = -1;
                _mines[minesPlanted] = new[] { x, y };
                minesPlanted++;
            }
        }

        private void GenerateClue()
        {
            foreach (var mine in _mines)
            {
                var x = mine[0];
                var y = mine[1];

                if (IsInBoard(x - 1, y - 1) && IsNotMine(_board[x - 1, y - 1])) _board[x - 1, y - 1]++;    //top-left
                if (IsInBoard(x - 1, y) && IsNotMine(_board[x - 1, y])) _board[x - 1, y]++;                  //top
                if (IsInBoard(x - 1, y + 1) && IsNotMine(_board[x - 1, y + 1])) _board[x - 1, y + 1]++;    //top-right
                if (IsInBoard(x, y + 1) && IsNotMine(_board[x, y + 1])) _board[x, y + 1]++;                  //right
                if (IsInBoard(x + 1, y + 1) && IsNotMine(_board[x + 1, y + 1])) _board[x + 1, y + 1]++;    //bottom-right
                if (IsInBoard(x + 1, y) && IsNotMine(_board[x + 1, y])) _board[x + 1, y]++;                  //bottom
                if (IsInBoard(x + 1, y - 1) && IsNotMine(_board[x + 1, y - 1])) _board[x + 1, y - 1]++;    //bottom-left
                if (IsInBoard(x, y - 1) && IsNotMine(_board[x, y - 1])) _board[x, y - 1]++;                  //left
            }
        }

        internal void Display()
        {
            for (var row = 0; row < _rows; row++)
            {
                for (var col = 0; col < _cols; col++)
                {
                    Console.Write("--");
                }
                Console.Write("-");
                Console.WriteLine();

                for (var col = 0; col < _cols; col++)
                {
                    Console.Write('|');
                    if (IsNotEmpty(_board[row, col]))
                    {
                        if (IsMine(_board[row, col]))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write('X');
                            Console.ResetColor();
                        }
                        else
                        {
                            Console.Write(_board[row, col]);
                        }
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                }
                Console.Write('|');
                Console.WriteLine();
            }

            for (var col = 0; col < _cols; col++)
            {
                Console.Write("--");
            }
            Console.Write("-");
            Console.WriteLine();
        }

        internal int[,] GetBoard()
        {
            return _board;
        }
    }
}
