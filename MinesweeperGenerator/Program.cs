﻿using System;

namespace MinesweeperGenerator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Minesweeper Generator");

            Console.Write("Rows (default is 10): ");
            if (!int.TryParse(Console.ReadLine(), out var rows))
            {
                rows = 10;
            }

            Console.Write("Cols (default is 20): ");
            if (!int.TryParse(Console.ReadLine(), out var cols))
            {
                cols = 20;
            }

            Console.Write("Mines (default is 15): ");
            if (!int.TryParse(Console.ReadLine(), out var minesCount))
            {
                minesCount = 15;
            }

            var game = new Game(rows, cols, minesCount);
            game.Display();
        }
    }
}