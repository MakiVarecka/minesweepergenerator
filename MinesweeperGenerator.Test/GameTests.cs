using System;
using Xunit;

namespace MinesweeperGenerator.Test
{
    public class GameTests
    {
        [Fact]
        public void GenerateMines_DifferentMinesCountThanInput_EqualAsInput()
        {
            const int minesCount = 15;
            var game = new Game(10, 20, minesCount);
            var board = game.GetBoard();


            var rows = board.GetLength(0);
            var cols = board.GetLength(1);

            var minesFound = 0;
            for (var x = 0; x < rows; x++)
            {
                for (var y = 0; y < cols; y++)
                {
                    if (board[x, y] == -1) minesFound++;
                }
            }

            Assert.Equal(minesCount, minesFound);
        }

        [Fact]
        public void Prepare_NumberOfRowsLessOrEqualToZero_Throw()
        {
            const int zeroRows = 0;
            const int negativeRows = -1;

            Assert.Throws<ArgumentException>(() => new Game(zeroRows, 15, 10));
            Assert.Throws<ArgumentException>(() => new Game(negativeRows, 15, 10));

        }

        [Fact]
        public void Prepare_NumberOfColsLessOrEqualToZero_Throw()
        {
            const int zeroCols = 0;
            const int negativeCols = -1;

            Assert.Throws<ArgumentException>(() => new Game(10, zeroCols, 10));
            Assert.Throws<ArgumentException>(() => new Game(10, negativeCols, 10));

        }

        [Fact]
        public void Prepare_NoMines_Throw()
        {
            const int minesCount = 0;

            Assert.Throws<ArgumentException>(() => new Game(10, 15, minesCount));
        }

        [Fact]
        public void Prepare_NumberOfMinesGreaterThanBoard_Throw()
        {
            const int rows = 10;
            const int cols = 15;
            const int minesCount = 200;

            Assert.Throws<ArgumentException>(() => new Game(rows, cols, minesCount));
        }
    }
}
