# Description #

My approach consists of 3 steps:

1. Prepere board (layout)
2. Generate mines
3. Generate clue

Preparing board is simple. It is just definition of variables.

For mine generation is use while cycle. 
Coordinates (x,y) are randomly generated each iteration. 
If there is no mine, then mine is planted and coordinates are written to mine position array (for next clue generation). 
If there is a mine, then new iteration starts until there is no mine left. 

Clue generator is based on previously saved mine positions. 
Surrounding around a mine is increased value of clue by 1 (only if it is no mine or index is not out of boundary).
